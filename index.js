//https://www.codingame.com/ide/puzzle/the-descent

while (true) {
    var maxHeight = 0;
    var imax = 0;
    for (var i = 0; i < 8; i++) {
        var mountainH = parseInt(readline()); // represents the height of one mountain.
        if (mountainH > maxHeight) {
            maxHeight = mountainH;
            imax = i;
        }
    }
    print(imax);
}
